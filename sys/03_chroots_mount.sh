#!/bin/bash

TBCOPIED="
  /etc/resolv.conf
  /etc/hostname
  "


mkdir -p /sys_pay/host > /dev/null 2>/dev/null || true
mkdir -p /sys_stg/host > /dev/null 2>/dev/null || true
mount --rbind / /sys_pay/host
mount --rbind / /sys_stg/host


for SUBROOT in stg pay
do

  mount --types proc /proc /sys_$SUBROOT/proc || true

  for BINDMOUNT in sys dev run var/run tmp home root dta mass media
  do
    if [ -d /$BINDMOUNT ] && [ ! -h /$BINDMOUNT ]
    then
      echo mount --rbind /$BINDMOUNT /sys_$SUBROOT/$BINDMOUNT || true
      mount --rbind /$BINDMOUNT /sys_$SUBROOT/$BINDMOUNT || true
      echo mount --make-rslave /sys_$SUBROOT/$BINDMOUNT || true
      mount --make-rslave /sys_$SUBROOT/$BINDMOUNT || true
    fi
  done

  for FILE in $TBCOPIED
  do
    if [ -f $FILE ]
    then
      echo install -b -D $FILE /sys_$SUBROOT/$FILE
      install -b -D $FILE /sys_$SUBROOT/$FILE
    fi
  done

done

mkdir -p /sys_stg/sys_pay > /dev/null 2>/dev/null || true
mount --rbind /sys_pay /sys_stg/sys_pay


TMP=`mktemp`
python3 - > $TMP <<END_OF_PYTHON_CODE

normative_hosts = None;
with open( "./chroots/etc/hosts", "rt" ) as f:
  normative_hosts = f.read();
normative_hosts = normative_hosts.replace( "__HOSTNAME__", "$HOSTNAME" );

etc_hosts = None;
with open( "/etc/hosts", "rt" ) as f:
  etc_hosts = f.read();

def parse_hosts( ip4_ip_by_host, ip6_ip_by_host, hosts_file_content ):

  for line in hosts_file_content.split( "\n" ):

    line = line.strip();

    line = line.split();
    if not line:
      continue;

    ip = line[0];

    for name in line[1:]:
      if "::" in ip:
        ip6_ip_by_host[ name ] = ip;
      else:
        ip4_ip_by_host[ name ] = ip;

ip4_ip_by_host = {};
ip6_ip_by_host = {};
parse_hosts( ip4_ip_by_host, ip6_ip_by_host, normative_hosts );
parse_hosts( ip4_ip_by_host, ip6_ip_by_host, etc_hosts );

names_by_ip = {};

for ( name, ip ) in ip6_ip_by_host.items():
  if ip not in names_by_ip:
    names_by_ip[ ip ] = [];
  names_by_ip[ ip ].append( name );

for ( name, ip ) in ip4_ip_by_host.items():
  if ip not in names_by_ip:
    names_by_ip[ ip ] = [];
  names_by_ip[ ip ].append( name );

import sys;

for ip in sorted( names_by_ip.keys() ):
  names = names_by_ip[ ip ];
  sys.stdout.write( ip + " " );
  for name in names:
    sys.stdout.write( name + " " );
  sys.stdout.write( "\n" );

END_OF_PYTHON_CODE

echo install -D $TMP /sys_stg/etc/hosts
install -D $TMP /sys_stg/etc/hosts
echo install -D $TMP /sys_pay/etc/hosts
install -D $TMP /sys_pay/etc/hosts


TMP_PASSWD=`mktemp`
TMP_GROUP=`mktemp`
TMP_SHADOW=`mktemp`

python3 - <<END_OF_PYTHON_CODE

shadow_by_name = {};

with open( "/etc/shadow", "rt", encoding="ascii" ) as f:
  for line in f:
    ( name, passwd, rest ) = line.split( ":", 2 );
    shadow_by_name[ name ] = line;

with open( "./chroots/etc/shadow", "rt", encoding="ascii" ) as f:
  for line in f:
    ( name, passwd, rest ) = line.split( ":", 2 );
    shadow_by_name[ name ] = line;

with open( "$TMP_SHADOW", "wt", encoding="ascii" ) as f_shadow:
  with open( "$TMP_PASSWD", "wt", encoding="ascii" ) as f_passwd:

    with open( "./chroots/etc/passwd", "rt" ) as f:
      for line in f:
        ( name, passwd, uid, gid, rest ) = line.split( ":", 4 );
        uid = int( uid );
        gid = int( gid );
        if not ( 1000 <= uid <= 65000 ):
          assert not ( 1000 <= gid <= 65000 );
          f_passwd.write( line );
          f_shadow.write( shadow_by_name[ name ] );

    with open( "/etc/passwd", "rt" ) as f:
      for line in f:
        ( name, passwd, uid, gid, rest ) = line.split( ":", 4 );
        uid = int( uid );
        gid = int( gid );
        if 1000 <= uid <= 65000:
          assert 1000 <= gid <= 65000;
          f_passwd.write( line );
          f_shadow.write( shadow_by_name[ name ] );

with open( "$TMP_GROUP", "wt", encoding="ascii" ) as f_group:

  with open( "./chroots/etc/group", "rt" ) as f:
    for line in f:
      ( group_name, passwd, gid, rest ) = line.split( ":", 3 );
      gid = int( gid );
      if not ( 1000 <= gid <= 65000 ):
        f_group.write( line );

  with open( "/etc/group", "rt" ) as f:
    for line in f:
      ( group_name, passwd, gid, rest ) = line.split( ":", 3 );
      gid = int( gid );
      if 1000 <= gid <= 65000:
        f_group.write( line );

END_OF_PYTHON_CODE

echo install -D $TMP_PASSWD /sys_stg/etc/passwd
install -D $TMP_PASSWD /sys_stg/etc/passwd
echo install -D $TMP_PASSWD /sys_pay/etc/passwd
install -D $TMP_PASSWD /sys_pay/etc/passwd

echo install -D $TMP_GROUP /sys_stg/etc/group
install -D $TMP_GROUP /sys_stg/etc/group
echo install -D $TMP_GROUP /sys_pay/etc/group
install -D $TMP_GROUP /sys_pay/etc/group

echo install -D $TMP_SHADOW /sys_stg/etc/shadow
install -D $TMP_SHADOW /sys_stg/etc/shadow
echo install -D $TMP_SHADOW /sys_pay/etc/shadow
install -D $TMP_SHADOW /sys_pay/etc/shadow
