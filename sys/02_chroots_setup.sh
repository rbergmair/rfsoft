#!/bin/bash

for SUBROOT in stg pay
do

  if [ -d chroots ]
  then
    pushd chroots
    for VAL in `find -type f | grep -v '.svn'`
    do
      echo install -b -D $VAL /sys_$SUBROOT/$VAL
      install -b -D $VAL /sys_$SUBROOT/$VAL
    done
    popd
  fi

done

chmod og-rwx /sys_stg/etc/ssh/sshd_config >/dev/null 2>/dev/null || true
chmod og-rwx /sys_pay/etc/ssh/sshd_config >/dev/null 2>/dev/null || true

echo "export PS1=\"(RFSOFT stg) \$PS1\"" > /sys_stg/etc/profile.d/ps.sh
echo "export PS1=\"(RFSOFT) \$PS1\"" > /sys_pay/etc/profile.d/ps.sh
