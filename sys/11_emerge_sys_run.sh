#!/bin/bash

PKGS1=""
PKGS1="$PKGS1 vanilla-sources linux-headers binutils gcc"

PKGS2=""
PKGS2="$PKGS2 glibc baselayout"

PKGS3=""
PKGS3="$PKGS3 tbb dev-vcs/git"

PKGS4=""
PKGS4="$PKGS4 coreutils diffutils file findutils gettext grep sed sys-apps/texinfo less lsof"
PKGS4="$PKGS4 bash perl gawk"
PKGS4="$PKGS4 app-arch/tar gzip bzip2 app-arch/xz-utils zip unzip"
PKGS4="$PKGS4 which procps"
PKGS4="$PKGS4 telnet-bsd net-ftp/ftp"
PKGS4="$PKGS4 sysstat"
PKGS4="$PKGS4 sudo"
PKGS4="$PKGS4 pam"
PKGS4="$PKGS4 libselinux"

PKGS4="$PKGS4 sys-process/at sys-process/daemontools"
PKGS4="$PKGS4 sys-apps/net-tools"

PKGS4="$PKGS4 make sys-devel/patch"
PKGS4="$PKGS4 dos2unix vim"
PKGS4="$PKGS4 app-misc/screen"

PKGS4="$PKGS4 lynx virtual/w3m"

PKGS4="$PKGS4 mail-client/mailx"

PKGS4="$PKGS4 openssh"

PKGS4="$PKGS4 wget"

PKGS4="$PKGS4 www-servers/nginx"
PKGS4="$PKGS4 app-admin/supervisor"

PKGS4="$PKGS4 dev-lang/python dev-python/pip"

PKGS4="$PKGS4 libxml2 libxslt"

PKGS4="$PKGS4 dev-libs/libevent"

PKGS4="$PKGS4 libaio"

PKGS4="$PKGS4 app-arch/snappy"
PKGS4="$PKGS4 dev-libs/jansson"
PKGS4="$PKGS4 dev-libs/protobuf"
PKGS4="$PKGS4 dev-libs/cyrus-sasl"

PKGS4="$PKGS4 rsync"

# prerequisites for imagemagick
PKGS5=""
PKGS5="$PKGS5 virtual/jpeg"
PKGS5="$PKGS5 media-libs/jbigkit"
PKGS5="$PKGS5 media-libs/libpng"
PKGS5="$PKGS5 media-libs/libjpeg-turbo"
PKGS5="$PKGS5 media-libs/tiff"
PKGS5="$PKGS5 media-libs/libwebp"
PKGS5="$PKGS5 media-libs/lcms"
PKGS5="$PKGS5 media-libs/openjpeg"

PKGS6=""
PKGS6="$PKGS6 dev-python/python-openstackclient app-admin/awscli"

ROOT="/"        emerge --verbose --update $1 $PKGS1 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS2 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS3 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS4 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS5 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS6 || exit

ROOT="/sys_pay" emerge --verbose --update $1 $PKGS1 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS2 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS3 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS4 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS5 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS6 || exit
