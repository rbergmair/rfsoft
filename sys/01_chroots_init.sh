#!/bin/bash

mkdir /sys_stg > /dev/null 2>/dev/null || true
mkdir /sys_pay > /dev/null 2>/dev/null || true

mkdir /dta > /dev/null 2>/dev/null || true
mkdir /mass > /dev/null 2>/dev/null || true

for DIR in proc sys dev etc etc/profile.d etc/ssh tmp dta mass media home root sw lib32 lib64 usr var usr/lib32 usr/lib64
do
  mkdir -p /sys_stg/$DIR > /dev/null 2>/dev/null || true
  mkdir -p /sys_pay/$DIR > /dev/null 2>/dev/null || true
done

if [ -d /var/run ] && [ -h /var/run ]  && [ -d /run ] && [ ! -h /run ]
then
  mkdir -p /sys_stg/run
  mkdir -p /sys_pay/run
  ln -s /run /sys_stg/var/run
  ln -s /run /sys_pay/var/run
fi

if [ -d /var/run ] && [ ! -h /var/run ]
then
  mkdir -p /sys_stg/var/run
  mkdir -p /sys_pay/var/run
fi

# UNAME=`uname -m`
# 
# if [ "x$UNAME" == "xx86_64" ]
# then
# 
#   ln -s lib64 /sys_stg/lib
#   ln -s lib64 /sys_stg/usr/lib
#   ln -s lib64 /sys_pay/lib
#   ln -s lib64 /sys_pay/usr/lib
# 
# else
# 
#   if [ "x$UNAME" == "xi686" ]
#   then
# 
#     ln -s lib32 /sys_stg/lib
#     ln -s lib32 /sys_stg/usr/lib
#     ln -s lib32 /sys_pay/lib
#     ln -s lib32 /sys_pay/usr/lib
# 
#   else
#     echo "can't figure this system out"
#     exit -2
#   fi
# 
# fi
