#!/bin/bash

TBCOPIED="
  /etc/resolv.conf
  /etc/hostname
  /etc/hosts
  /etc/passwd
  /etc/group
  /etc/shadow
  /etc/ssh/*key*
  "

for SUBROOT in stg pay
do

  for FILE in $TBCOPIED
  do
    if [ -f $FILE ]
    then
      echo rm -f /sys_$SUBROOT/$FILE
      rm -f /sys_$SUBROOT/$FILE
    fi
  done

  pushd /sys_$SUBROOT/sw/Python*/

  find -type f -name '*.pyc' | while read VAL; do echo rm -f $VAL; rm -f $VAL; done
  find -type d -name '__pycache__' | while read VAL; do echo rmdir $VAL; rmdir $VAL; done

  popd

done

./02_chroots_setup.sh
