#!/bin/bash

for PREFIX in / /sys_pay/
do

  echo_supervisord_conf > ${PREFIX}etc/supervisord.conf
  echo "[include]" >> ${PREFIX}etc/supervisord.conf
  echo "files = /etc/supervisor/conf.d/*.conf /sw/etc/*/supervisor-conf.d/*.conf /root/supervisor-conf.d/*.conf /home/pay/supervisor-conf.d/*.conf" >> ${PREFIX}etc/supervisord.conf
  rm -rf ${PREFIX}etc/supervisor
  mkdir -p ${PREFIX}etc/supervisor/conf.d

done

for PREFIX in / /sys_pay/
do

  if [ -d chroots ]
  then
    pushd chroots
    for VAL in `find -type f | grep -v '.svn'`
    do
      echo install -b -D $VAL ${PREFIX}${VAL}
      install -b -D $VAL ${PREFIX}${VAL}
    done
    popd
  fi

done

chmod og-rwx /etc/ssh/sshd_config
chmod og-rwx /sys_pay/etc/ssh/sshd_config
