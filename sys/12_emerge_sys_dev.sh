#!/bin/bash

PKGS1=
PKGS1="$PKGS1 app-crypt/gnupg"
PKGS1="$PKGS1 indent"
PKGS1="$PKGS1 m4"
PKGS1="$PKGS1 dev-vcs/subversion"

PKGS2=
PKGS2="$PKGS2 setuptools docutils pygments"

PKGS3=
PKGS3="$PKGS3 dev-python/sphinx dev-python/flake8"
PKGS3="$PKGS3 dev-python/flake8"

PKGS4=
PKGS4="$PKGS4 ruby rubygems nodejs"
PKGS4="$PKGS4 pandoc"

ROOT="/"        emerge --verbose --update $1 $PKGS1 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS2 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS3 || exit
ROOT="/"        emerge --verbose --update $1 $PKGS4 || exit

ROOT="/sys_pay" emerge --verbose --update $1 $PKGS1 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS2 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS3 || exit
ROOT="/sys_pay" emerge --verbose --update $1 $PKGS4 || exit
