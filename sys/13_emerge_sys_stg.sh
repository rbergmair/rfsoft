#!/bin/bash

PKGS=

# PKGS="$PKGS dev-vcs/subversion dev-vcs/bzr dev-vcs/mercurial dev-vcs/cvs"
PKGS="$PKGS dev-vcs/subversion"
# PKGS="$PKGS dev-vcs/bzr"
PKGS="$PKGS dev-vcs/mercurial"
PKGS="$PKGS dev-vcs/cvs"

PKGS="$PKGS dev-lang/swig"
PKGS="$PKGS dev-util/scons"
PKGS="$PKGS dev-util/cmake"

# PKGS="$PKGS sys-power/cpufrequtils"

PKGS="$PKGS net-analyzer/netcat"

ROOT="/" emerge --verbose --update $1 $PKGS
