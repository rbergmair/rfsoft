# About RFSOFT

RFSOFT aims to balance the following requirements, in order of decreasing
priority:

- **FULL CONTROL AND AUDITABILITY:**  For payload software inside RFSOFT, we
  want to put ourselves in a position of being able to know exactly and be
  able to have full control over what packages we are running, and how they
  are getting built from source.  The source code, including the build
  process, needs to be fully auditable.  No automatic pulling in of
  dependencies off the internet please.  No binary packages please, unless
  there really is no other way (e.g. [Oracle Java](http://java.oracle.com/)).
- **COMPLETE:**  You should be able to deploy this, and just run any
  software (designed to work in RFSOFT) out of the box.  Also, you should have
  all the tools at your disposal that you require for development, debugging,
  operational support, etc.  If you find yourself having to install additional
  packages (which you can't because there's no package manager, and in
  operational environments, there often is no internet), then find a RFSOFT
  maintainer, and ask for the software you need to be added to RFSOFT.
- **CONSISTENT:**  However, rather than trying to accommodate everyone's
  subjective tastes, we will try to converge on doing the
  [THE RIGHT THING](http://www.catb.org/jargon/html/R/Right-Thing.html),
  and the right thing only.  If you want to develop software to run in RFSOFT,
  start by using packages that are already there, and ask for new dependencies
  to be added to RFSOFT only when that is not an option.
- **SMALL:**  The binary image will need to be able to make its way through
  weirdly set-up multi-hop encryption into environments which don't have
  internet access etc. etc.  So let's try to keep it small.  On the other
  hand, we can safely assume that this thing will be running on beefy
  hardware, and that we don't need to get so fanatical about wanting to be
  small that we would want to start considering
  [BusyBox](https://busybox.net/) et al.
- **EASY TO MAINTAIN:**  For RFSOFT, "user friendliness" should not be an
  acceptable reason to add to the pain of RFSOFT maintenance.  No IDEs.
  No GUIs.  No [bloat](https://en.wikipedia.org/wiki/Software_bloat).
  Just tools which
  [do one thing and
  do it well](https://en.wikipedia.org/wiki/Unix_philosophy).


RFSOFT distinguishes between

* payload software under `/sw`
* system software under `/`

Payload software is software that we directly interface with from the point of
view of the code we actually write ourselves.  This includes, for example, all
the [python](http://www.python.org/) APIs that we actually use, but it also
includes things like databases, etc.

In the case of [python](http://www.python.org/) libraries, it also includes
all direct or indirect dependencies thereof (i.e. the entire python
ecosystem).  In cases where a python package is only a wrapper around a native
library or program, then such software is also considered payload software.

For example, we rely on a number of packages which in turn rely on
[numpy](http://www.numpy.org/), so that's why numpy is considered payload
software.  And since numpy requires
[atlas](http://math-atlas.sourceforge.net/), or a similar linear algebra
library, that's considered payload software too.  We might also want to do
stuff with images, which is why we have
[Wand](https://pypi.python.org/pypi/Wand).
But that is just a python frontend to
[ImageMagick](http://www.imagemagick.org/script/index.php), so
ImageMagick
is considered payload software as well.  ImageMagick in turn requires
[libpng](http://libpng.org/pub/png/libpng.html), but since libpng is
already two steps removed in terms of the dependency tree from the
Wand library that our own code actually interaces with, libpng is
no longer considered payload software, but rather it's system software.

Another example of system software would be the text editor `vi`.  We
want to have that in RFSOFT, because it's useful for development and
operations.  But we're not going to write complex code that interfaces
with `vi` in any way.  We just want to use it, and we preferably want
to get the newest version whenever we do a rebuild of RFSOFT.  So that's
why `vi` is system software, and not payload software.
