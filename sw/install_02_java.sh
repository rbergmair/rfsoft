#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/java

mkdir -p /sw/jlib > /dev/null 2> /dev/null || true
mkdir -p /sys_pay/sw/jlib > /dev/null 2> /dev/null || true

ARCHEDPKGS=

ARCHEDPKGS="$ARCHEDPKGS httpcomponents-client-4.5.6-bin.tar.gz"
ARCHEDPKGS="$ARCHEDPKGS commons-lang3-3.7-bin.tar.gz"
ARCHEDPKGS="$ARCHEDPKGS jeromq-0.4.3.tar.gz"
ARCHEDPKGS="$ARCHEDPKGS bouncycastle-160.tar.gz"

for ARCHEDPKG in $ARCHEDPKGS
do
  tar xfz $_PKGSRC/$ARCHEDPKG -C /sw/jlib || exit -1
  tar xfz $_PKGSRC/$ARCHEDPKG -C /sys_pay/sw/jlib || exit -1
done

./java/vnu-18.7.23.sh
