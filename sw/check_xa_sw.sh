#!/bin/bash

source /sw/sw

[ "x`/sw/sw which magick`"       == "x/sw/ImageMagick-7.0.7-35/bin/magick" ]   || { echo "!" && exit -1; };
[ "x`/sw/sw which python3`"      == "x/sw/Python-3.6.5/bin/python3" ]          || { echo "!" && exit -1; };
[ "x`/sw/sw which mpmetis`"      == "x/sw/SuiteSparse-5.2.0/bin/mpmetis" ]     || { echo "!" && exit -1; };
[ "x`/sw/sw which ogr2ogr`"      == "x/sw/gdal-2.3.0/bin/ogr2ogr" ]            || { echo "!" && exit -1; };
[ "x`/sw/sw which java`"         == "x/sw/icedtea-bin-3.8.0-amd64/bin/java" ]  || { echo "!" && exit -1; };
[ "x`/sw/sw which kcutilmgr`"    == "x/sw/kyotocabinet-1.2.76/bin/kcutilmgr" ] || { echo "!" && exit -1; };
[ "x`/sw/sw which memcached`"    == "x/sw/memcached-1.5.8/bin/memcached" ]     || { echo "!" && exit -1; };
[ "x`/sw/sw which nim`"          == "x/sw/nim-0.18.0/bin/nim" ]                || { echo "!" && exit -1; };
[ "x`/sw/sw which koch`"         == "x/sw/nim-0.18.0/koch" ]                   || { echo "!" && exit -1; };
[ "x`/sw/sw which psql`"         == "x/sw/postgresql-10.4/bin/psql" ]          || { echo "!" && exit -1; };
[ "x`/sw/sw which proj`"         == "x/sw/proj-5.1.0/bin/proj" ]               || { echo "!" && exit -1; };
[ "x`/sw/sw which curve_keygen`" == "x/sw/zeromq-4.2.5/bin/curve_keygen" ]     || { echo "!" && exit -1; };
[ "x`/sw/sw which vnu`"          == "x/sw/vnu-18.7.23/bin/vnu" ]               || { echo "!" && exit -1; };
