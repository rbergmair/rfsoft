#!/bin/bash

CHECK_FILES="
  /sw/jlib/httpcomponents-client-4.5.6/lib/httpclient-4.5.6.jar
  /sw/jlib/commons-lang3-3.7/commons-lang3-3.7.jar
  /sw/jlib/jeromq-0.4.3/jeromq-0.4.3.jar
  /sw/jlib/bouncycastle-160/bcprov-jdk15on-160.jar
  /sw/vnu-18.7.23/vnu.jar
  "

for FILE in $CHECK_FILES
do
  if [ ! -e $FILE ]
  then
    echo "$FILE missing"
    exit -1
  fi
done
