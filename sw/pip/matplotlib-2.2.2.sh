#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

PKG_CONFIG_PATH=/sw/freetype-2.9.1/lib/pkgconfig \
  $_PIP install \
    --global-option=build_ext \
    --global-option="-R/sw/freetype-2.9.1/lib" \
    --global-option="-L/sw/freetype-2.9.1/lib" \
    --global-option="-I/sw/freetype-2.9.1/include" \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/matplotlib-2.2.2.tar.gz

PKG_CONFIG_PATH=/sw/freetype-2.9.1/lib/pkgconfig \
  $_PIP install \
    --root=/sys_pay \
    --global-option=build_ext \
    --global-option="-R/sw/freetype-2.9.1/lib" \
    --global-option="-L/sw/freetype-2.9.1/lib" \
    --global-option="-I/sw/freetype-2.9.1/include" \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/matplotlib-2.2.2.tar.gz
