#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

B=`mktemp -d`

tar xfz $_PKGSRC/pandas-0.23.1.tar.gz -C $B

pushd $B/pandas-0.23.1

$_PYTHON ./setup.py build
$_PYTHON ./setup.py install
$_PYTHON ./setup.py install --root=/sys_pay

popd

rm -rf $B
