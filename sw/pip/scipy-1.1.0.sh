#!/bin/bash

# 
# see
#
#   http://www.scipy.org/scipylib/building/linux.html
#   http://wiki.scipy.org/Installing_SciPy/Linux
#

source globals.sh
_PKGSRC=$_PKGSRC/pip

B=`mktemp -d`

tar xfz $_PKGSRC/scipy-1.1.0.tar.gz -C $B

export BLAS=/sw/atlas-3.10.3/lib/libptcblas.a
export LAPACK=/sw/atlas-3.10.3/lib/liblapack.a
export ATLAS=/sw/atlas-3.10.3/lib/libtatlas.so
export PATH=$_PYTHON_PREFIX/bin:$PATH

pushd $B/scipy-1.1.0/

$_PYTHON ./setup.py build
$_PYTHON ./setup.py install
$_PYTHON ./setup.py install --root=/sys_pay

popd

rm -rf $B
