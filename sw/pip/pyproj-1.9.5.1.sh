#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

PROJ_DIR=/sw/proj-5.1.0 \
  /sw/Python-3.6.5/bin/pip3.6 install \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      ~/rfsoft/pkgsrc/pip/pyproj-1.9.5.1.tar.gz

PROJ_DIR=/sw/proj-5.1.0 \
  $_PIP install \
    --root=/sys_pay \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/pyproj-1.9.5.1.tar.gz
