#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

B=`mktemp -d`

tar xfz $_PKGSRC/pyzmq-17.0.0.tar.gz -C $B

pushd $B/pyzmq-17.0.0

$_PYTHON ./setup.py build --zmq=/sw/zeromq-4.2.5
$_PYTHON ./setup.py install
$_PYTHON ./setup.py install --root=/sys_pay

popd

rm -rf $B
