#!/bin/bash

#
# see
#
#   http://www.scipy.org/scipylib/building/linux.html
#

source globals.sh
_PKGSRC=$_PKGSRC/pip

B=`mktemp -d`

unzip $_PKGSRC/numpy-1.14.4.zip -d $B || exit -1

pushd $B/numpy-1.14.4/

echo > site.cfg

echo '[DEFAULT]' >> site.cfg
echo 'library_dirs = /sw/Python-3.6.5/lib, /sw/atlas-3.10.3/lib, /sw/SuiteSparse-5.2.0/lib' >> site.cfg
echo 'include_dirs = /sw/Python-3.6.5/include, /sw/atlas-3.10.3/include, /sw/SuiteSparse-5.2.0/include' >> site.cfg
echo '[blas_opt]' >> site.cfg
echo 'libraries = ptf77blas, ptcblas, atlas' >> site.cfg
echo '[lapack_opt]' >> site.cfg
echo 'libraries = lapack, ptf77blas, ptcblas, atlas' >> site.cfg
echo '[amd]' >> site.cfg
echo 'amd_libs = amd' >> site.cfg
echo '[umfpack]' >> site.cfg
echo 'umfpack_libs = umfpack' >> site.cfg

$_PYTHON ./setup.py build || exit -1
$_PYTHON ./setup.py install || exit -1
$_PYTHON ./setup.py install --root=/sys_pay || exit -1

popd

rm -rf $B
