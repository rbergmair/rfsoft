#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

$_PIP install \
  --global-option=build_ext \
  --global-option="-R/sw/leveldb-1.20/out-shared" \
  --global-option="-L/sw/leveldb-1.20/out-shared" \
  --global-option="-I/sw/leveldb-1.20/include" \
  --ignore-installed \
  --force-reinstall \
  --no-index \
  --no-deps \
    $_PKGSRC/plyvel-1.0.4.tar.gz

$_PIP install \
  --root=/sys_pay \
  --global-option=build_ext \
  --global-option="-R/sw/leveldb-1.20/out-shared" \
  --global-option="-L/sw/leveldb-1.20/out-shared" \
  --global-option="-I/sw/leveldb-1.20/include" \
  --ignore-installed \
  --force-reinstall \
  --no-index \
  --no-deps \
    $_PKGSRC/plyvel-1.0.4.tar.gz
