#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

GEOS_DIR=/sw/geos-3.6.2 \
  $_PIP install \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/basemap-1.1.0.tar.gz

GEOS_DIR=/sw/geos-3.6.2 \
  $_PIP install \
    --root=/sys_pay \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/basemap-1.1.0.tar.gz
