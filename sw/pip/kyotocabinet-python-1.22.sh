#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

$_PIP install \
  --global-option=build_ext \
  --global-option="-R/sw/kyotocabinet-1.2.76/lib" \
  --global-option="-L/sw/kyotocabinet-1.2.76/lib" \
  --global-option="-I/sw/kyotocabinet-1.2.76/include" \
  --ignore-installed \
  --force-reinstall \
  --no-index \
  --no-deps \
    $_PKGSRC/kyotocabinet-python-1.22.tar.gz

$_PIP install \
  --root=/sys_pay \
  --global-option=build_ext \
  --global-option="-R/sw/kyotocabinet-1.2.76/lib" \
  --global-option="-L/sw/kyotocabinet-1.2.76/lib" \
  --global-option="-I/sw/kyotocabinet-1.2.76/include" \
  --ignore-installed \
  --force-reinstall \
  --no-index \
  --no-deps \
    $_PKGSRC/kyotocabinet-python-1.22.tar.gz
