#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

GDAL_CONFIG=/sw/gdal-2.3.0/bin/gdal-config \
  $_PIP install \
    --global-option=build_ext \
    --global-option="-R/sw/gdal-2.3.0/lib" \
    --global-option="-L/sw/gdal-2.3.0/lib" \
    --global-option="-I/sw/gdal-2.3.0/include" \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/Fiona-1.7.11.post2.tar.gz

GDAL_CONFIG=/sw/gdal-2.3.0/bin/gdal-config \
  $_PIP install \
    --root=/sys_pay \
    --global-option=build_ext \
    --global-option="-R/sw/gdal-2.3.0/lib" \
    --global-option="-L/sw/gdal-2.3.0/lib" \
    --global-option="-I/sw/gdal-2.3.0/include" \
    --ignore-installed \
    --force-reinstall \
    --no-index \
    --no-deps \
      $_PKGSRC/Fiona-1.7.11.post2.tar.gz
