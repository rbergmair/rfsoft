#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

B=`mktemp -d`

tar xfz $_PKGSRC/Shapely-1.6.4.post1.tar.gz -C $B

export GEOS_CONFIG=/sw/geos-3.6.2/bin/geos-config

pushd $B/Shapely-1.6.4.post1/

pushd shapely
mv geos.py geos.py.orig
head -n27 geos.py.orig >> geos.py
echo '    lib = find_library(libname) if libname != "geos_c" else "/sw/geos-3.6.2/lib/libgeos_c.so";' >> geos.py
tail -n820 geos.py.orig >> geos.py
diff geos.py.orig geos.py
popd

$_PYTHON ./setup.py build_ext \
  -R/sw/geos-3.6.2/lib \
  -L/sw/geos-3.6.2/lib \
  -I/sw/geos-3.6.2/include

$_PYTHON ./setup.py build
$_PYTHON ./setup.py install
$_PYTHON ./setup.py install --root=/sys_pay

popd

rm -rf $B
