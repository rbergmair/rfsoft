#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

B=`mktemp -d`

tar xfz $_PKGSRC/GDAL-2.3.0.tar.gz -C $B

pushd $B/GDAL-2.3.0/

echo > setup.cfg

echo '[egg_info]' >> setup.cfg
echo 'tag_build =' >> setup.cfg
echo 'tag_date = 0' >> setup.cfg
echo 'tag_svn_revision = 0' >> setup.cfg
echo '' >> setup.cfg
echo '[build_ext]' >> setup.cfg
echo 'gdal_config = /sw/gdal-2.3.0/bin/gdal-config' >> setup.cfg

$_PYTHON \
  ./setup.py build_ext \
    -R/sw/gdal-2.3.0/lib \
    -L/sw/gdal-2.3.0/lib \
    -I/sw/gdal-2.3.0/include

$_PYTHON ./setup.py build
$_PYTHON ./setup.py install
$_PYTHON ./setup.py install --root=/sys_pay

popd

rm -rf $B
