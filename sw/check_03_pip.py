# very fundamental stuff!
import setuptools;
import setuptools_scm;
import docutils;
import cython;

# generally useful stuff that's light in dependencies
import click;
import hashids;
import decorator;
import termcolor;
import pytz;
import dateutil;

# generally useful stuff that's light in dependencies (and are deps of pytest)
import attr;
import more_itertools;
import atomicwrites;
import pluggy;

# unit/coverage testing
import py;
import py.test;
import pytest;
import coverage;
import nose;

# useful minimalistic libs for dealing with data in simple formats
import pyparsing;
import yaml;
import msgpack;

# web server, often comes in handy!
import tornado;

# minimalist client/server key-value store
import pymemcache;

# minimalist lib-based key-value stores
import kyotocabinet;
import plyvel;

# minimalist message passing lib
import zmq;

# useful for web scraping and the like
## python-requests plus dependencies
import dns;
import urllib3;
import idna;
import chardet;
import certifi;
import requests;
## beautiful soup dependencies
import webencodings;
import six;
import html5lib;
import lxml;
## useful css stuff
import cssselect;
import cssutils;
## beautiful soup
import bs4;

# useful for doing stuff with Excel files 
import jdcal;
import xlrd;
import openpyxl;

# useful for image processing (ImageMagick API)
import wand;

# useful for formatting reports etc
import markupsafe;
import jinja2;
import textile;
import reportlab;

# prerequisites for stage-1 scientific python stuff
import cycler;
import kiwisolver;

# stage-1 scientific python stuff including geographical stuff
import numpy;
import scipy;

from osgeo import gdal;
from osgeo import ogr;
from osgeo import osr;
from osgeo import gdal_array;
from osgeo import gdalconst;

import shapely;
from shapely.geometry import Point;
import fiona;
import pyproj;
# pyproj.test();

import matplotlib;

import mpl_toolkits.basemap;

# scientific python stuff stage 2
import pandas;

# scientific python stuff stage 2 (cont'd)
import patsy;
import sklearn;

# scientific python stuff stage 3

# nlp stuff
import Stemmer;
import nltk;

# network analysis stuff
import networkx;

# geographical stuff
import descartes;
import geopandas;
import pysal;

from pylab import *;
