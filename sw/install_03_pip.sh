#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/pip

PKGS=



# very fundamental stuff!
PKGS="$PKGS setuptools-39.2.0.zip"
PKGS="$PKGS setuptools_scm-2.1.0.tar.gz"
PKGS="$PKGS docutils-0.14.tar.gz"
PKGS="$PKGS Cython-0.28.3.tar.gz"

# generally useful stuff that's light in dependencies
PKGS="$PKGS click-6.7.tar.gz"
PKGS="$PKGS hashids-1.2.0.tar.gz"
PKGS="$PKGS decorator-4.3.0.tar.gz"
PKGS="$PKGS termcolor-1.1.0.tar.gz"
PKGS="$PKGS pytz-2018.4.tar.gz"
PKGS="$PKGS python-dateutil-2.7.3.tar.gz"

# generally useful stuff that's light in dependencies (and are deps of pytest)
PKGS="$PKGS attrs-18.2.0.tar.gz more-itertools-4.3.0.tar.gz atomicwrites-1.2.1.tar.gz pluggy-0.6.0.tar.gz"

# unit/coverage testing
PKGS="$PKGS py-1.5.3.tar.gz pytest-3.6.1.tar.gz pytest-runner-4.2.tar.gz"
PKGS="$PKGS coverage-4.5.1.tar.gz"
PKGS="$PKGS nose-1.3.7.tar.gz"

# useful minimalistic libs for dealing with data in simple formats
PKGS="$PKGS pyparsing-2.2.0.tar.gz"
PKGS="$PKGS PyYAML-3.12.tar.gz"
PKGS="$PKGS msgpack-0.5.6.tar.gz"

# web server, often comes in handy!
PKGS="$PKGS tornado-5.0.2.tar.gz"

# minimalist client/server key-value store
PKGS="$PKGS pymemcache-1.4.4.tar.gz"

for PKG in $PKGS
do
  $_PIP install --ignore-installed --force-reinstall --no-index --no-deps $_PKGSRC/$PKG || exit -1
  $_PIP install --root=/sys_pay --ignore-installed --force-reinstall --no-index --no-deps $_PKGSRC/$PKG || exit -1
done
PKGS=

# minimalist lib-based key-value stores
./pip/kyotocabinet-python-1.22.sh
./pip/plyvel-1.0.4.sh

# minimalist message passing lib
./pip/pyzmq-17.0.0.sh

# useful for web scraping and the like
## python-requests plus dependencies
PKGS="$PKGS dnspython-1.15.0.zip"
PKGS="$PKGS urllib3-1.22.tar.gz"
PKGS="$PKGS idna-2.6.tar.gz"
PKGS="$PKGS chardet-3.0.4.tar.gz"
PKGS="$PKGS certifi-2018.4.16.tar.gz"
PKGS="$PKGS requests-2.18.4.tar.gz"
## beautiful soup dependencies
PKGS="$PKGS webencodings-0.5.1.tar.gz"
PKGS="$PKGS six-1.11.0.tar.gz"
PKGS="$PKGS html5lib-1.0.1.tar.gz"
PKGS="$PKGS lxml-4.2.1.tar.gz"
## useful css stuff
PKGS="$PKGS cssselect-1.0.3.tar.gz"
PKGS="$PKGS cssutils-1.0.2.tar.gz"
## beautiful soup
PKGS="$PKGS beautifulsoup4-4.6.0.tar.gz"

# useful for doing stuff with Excel files 
PKGS="$PKGS jdcal-1.4.tar.gz"
PKGS="$PKGS xlrd-1.1.0.tar.gz"
PKGS="$PKGS openpyxl-2.5.4.tar.gz"

# useful for image processing (ImageMagick API)
PKGS="$PKGS Wand-0.4.4.tar.gz"

# useful for formatting reports etc
PKGS="$PKGS MarkupSafe-1.0.tar.gz"
PKGS="$PKGS Jinja2-2.10.tar.gz"
PKGS="$PKGS textile-3.0.3.tar.gz"
PKGS="$PKGS reportlab-3.4.0.tar.gz"

# prerequisites for stage-1 scientific python stuff
PKGS="$PKGS cycler-0.10.0.tar.gz"
PKGS="$PKGS kiwisolver-1.0.1.tar.gz"

for PKG in $PKGS
do
  $_PIP install --ignore-installed --force-reinstall --no-index --no-deps $_PKGSRC/$PKG || exit -1
  $_PIP install --root=/sys_pay --ignore-installed --force-reinstall --no-index --no-deps $_PKGSRC/$PKG || exit -1
done
PKGS=

# stage-1 scientific python stuff including geographical stuff
./pip/numpy-1.14.4.sh       || true
./pip/scipy-1.1.0.sh        || true

./pip/gdal-2.3.0.sh         || true

./pip/shapely-1.6.4.sh      || true
./pip/fiona-1.7.11.sh       || true
./pip/pyproj-1.9.5.1.sh     || true

./pip/matplotlib-2.2.2.sh   || true

./pip/basemap-1.1.0.sh      || true

# scientific python stuff stage 2
./pip/pandas-0.23.1.sh       || true

# scientific python stuff stage 2 (cont'd)
PKGS="$PKGS patsy-0.5.0.tar.gz"
PKGS="$PKGS scikit-learn-0.19.1.tar.gz"

# scientific python stuff stage 3

# nlp stuff
PKGS="$PKGS PyStemmer-1.3.0.tar.gz"
PKGS="$PKGS nltk-3.3.0.zip"

# network analysis stuff
PKGS="$PKGS networkx-2.1.zip"

# geographical stuff
PKGS="$PKGS descartes-1.1.0.tar.gz"
PKGS="$PKGS geopandas-0.3.0.tar.gz"
PKGS="$PKGS PySAL-1.14.3.tar.gz"

for PKG in $PKGS
do
  $_PIP install --ignore-installed --force-reinstall --no-index --no-deps $_PKGSRC/$PKG || exit -1
  $_PIP install --root=/sys_pay --ignore-installed --force-reinstall --no-index --no-deps $_PKGSRC/$PKG || exit -1
done
PKGS=
