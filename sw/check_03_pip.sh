#!/bin/bash

CHECK_FILES="
  /sw/Python-3.6.5/bin/coverage
  /sw/Python-3.6.5/bin/nosetests
  "

for FILE in $CHECK_FILES
do
  if [ ! -e $FILE ]
  then
    echo "$FILE missing"
    exit -1
  fi
done

/sw/Python-3.6.5/bin/python3 ./check_03_pip.py
