#!/bin/bash

# needed for scientific python
./native/atlas3.10.3.sh || exit -1
./native/SuiteSparse-5.10.1.sh || exit -1
./native/freetype-2.11.0.sh || exit -1

# geographical stuff
./native/proj-8.1.1.sh || exit -1
./native/gdal-3.4.0.sh || exit -1
./native/geos-3.9.2.sh || exit -1

# useful minimalistic stuff
./native/ImageMagick-6.9.12-31.sh || exit -1
./native/ImageMagick-7.1.0-16.sh || exit -1
./native/memcached-1.6.12.sh || exit -1
./native/google-leveldb-1.23.sh || exit -1
./native/kyotocabinet-1.2.79.sh || exit -1
./native/zeromq-4.3.4.sh || exit -1

# postgresql and extensions
./native/postgresql-10.19.sh || exit -1
./native/postgresql-14.1.sh || exit -1
./native/citus-8.3.2-pg10.sh || exit -1
./native/citus-10.2.2-pg14.sh || exit -1
./native/postgis-3.1.4.sh || exit -1

# java
./native/jdk-17_linux-x64_bin.sh || exit -1
./native/icedtea-bin-core-3.16.0-amd64.sh || exit -1

# nim
./native/nim-0.16.0.sh || exit -1
./native/nim-0.18.0.sh || exit -1
./native/nim-0.19.4.sh || exit -1
./native/nim-1.4.8.sh || exit -1

# ink
./native/Ink-3.1.10.sh || exit -1

# ...and of course
./native/Python-3.10.0.sh || exit -1
