#!/bin/bash

CHECK_FILES="
  /sw/atlas-3.10.3/lib/libsatlas.so
  /sw/atlas-3.10.3/lib/libtatlas.so
  /sw/atlas-3.10.3/lib/libatlas.a
  /sw/atlas-3.10.3/lib/libcblas.a
  /sw/atlas-3.10.3/lib/libf77blas.a
  /sw/atlas-3.10.3/lib/liblapack.a
  /sw/atlas-3.10.3/lib/libptcblas.a
  /sw/atlas-3.10.3/lib/libptf77blas.a
  /sw/SuiteSparse-5.10.1/lib/libamd.so
  /sw/freetype-2.11.0/lib/libfreetype.a
  /sw/proj-8.1.1/lib/libproj.so
  /sw/gdal-3.4.0/lib/libgdal.so
  /sw/geos-3.9.2/lib/libgeos.a
  /sw/ImageMagick-6.9.12-31/lib/libMagickCore-6.Q16.a
  /sw/ImageMagick-7.1.0-16/lib/libMagickCore-7.Q16HDRI.a
  /sw/memcached-1.6.12/bin/memcached
  /sw/google-leveldb-1.23/build/libleveldb.a
  /sw/kyotocabinet-1.2.79/lib/libkyotocabinet.so.16.14.0
  /sw/zeromq-4.3.4/lib/libzmq.a
  /sw/postgresql-10.19/bin/postgres
  /sw/postgresql-10.19/lib/citus.so
  /sw/postgresql-10.19/lib/postgis-3.so
  /sw/postgresql-14.1/bin/postgres
  /sw/postgresql-14.1/lib/citus.so
  /sw/postgresql-14.1/lib/postgis-3.so
  /sw/jdk-17.0.1/bin/java
  /sw/jdk-17.0.1/bin/javac
  /sw/icedtea-bin-3.16.0-amd64/bin/java
  /sw/icedtea-bin-3.16.0-amd64/bin/javac
  /sw/nim-0.16.0/bin/nim
  /sw/nim-0.16.0/koch
  /sw/nim-0.18.0/bin/nim
  /sw/nim-0.18.0/koch
  /sw/nim-0.19.4/bin/nim
  /sw/nim-0.19.4/koch
  /sw/nim-1.4.8/bin/nim
  /sw/nim-1.4.8/koch
  /sw/Ink-3.1.10/Ink-3.1.10.zip
  /sw/Python-3.10.0/bin/python3.10
  "

for FILE in $CHECK_FILES
do
  if [ ! -e $FILE ]
  then
    echo "$FILE missing"
    exit -1
  fi
done
