#!/bin/bash

for PREFIX in / /sys_pay/
do

  if [ -d chroots ]
  then
    pushd chroots
    for VAL in `find -type f | grep -v '.svn'`
    do
      echo install -b -D $VAL ${PREFIX}${VAL}
      install -b -D $VAL ${PREFIX}${VAL}
    done
    popd
  fi

done

echo chmod a+x /sw/sw
chmod a+x /sw/sw

echo chmod a+x /sys_pay/sw/sw
chmod a+x /sys_pay/sw/sw

echo chmod a+x /sw/vnu-18.7.23/bin/vnu
chmod a+x /sw/vnu-18.7.23/bin/vnu

echo chmod a+x /sys_pay/sw/vnu-18.7.23/bin/vnu
chmod a+x /sys_pay/sw/vnu-18.7.23/bin/vnu
