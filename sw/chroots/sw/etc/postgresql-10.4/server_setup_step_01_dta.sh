#!/bin/bash

chown -R daemon.daemon /sw/postgresql-10.4 || true

if [ ! -d /dta/postgresql_ ]
then
  mkdir -p /dta/postgresql_
fi

chown -R daemon.daemon /dta/postgresql_ || true
chmod 0700 /dta/postgresql_ || true

if [ ! -f /dta/postgresql_/PG_VERSION ]
then

sudo -u daemon \
  env \
      PATH=/sw/postgresql-10.4/bin:$PATH \
    initdb \
      -D /dta/postgresql_

fi
