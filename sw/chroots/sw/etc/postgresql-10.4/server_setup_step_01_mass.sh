#!/bin/bash

chown -R daemon.daemon /sw/postgresql-10.4 || true

if [ ! -d /mass/postgresql_ ]
then
  mkdir -p /mass/postgresql_
fi

chown -R daemon.daemon /mass/postgresql_ || true
chmod 0700 /mass/postgresql_ || true

if [ ! -f /mass/postgresql_/PG_VERSION ]
then

sudo -u daemon \
  env \
      PATH=/sw/postgresql-10.4/bin:$PATH \
    initdb \
      -D /mass/postgresql_

fi
