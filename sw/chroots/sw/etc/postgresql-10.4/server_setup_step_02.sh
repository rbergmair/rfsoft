#!/bin/bash

/sw/postgresql-10.4/bin/psql \
  postgres \
  -c "CREATE USER pay WITH SUPERUSER PASSWORD 'pay'" \
  || true;

/sw/postgresql-10.4/bin/psql \
  postgres \
  -U "pay" \
  -c "CREATE DATABASE pay" \
  || true;
