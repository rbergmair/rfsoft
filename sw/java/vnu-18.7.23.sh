#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/java

B=`mktemp -d`

unzip $_PKGSRC/vnu.jar_18.7.23.zip -d $B || exit -1

pushd $B

rm -rf /sw/vnu-18.7.23
cp -r dist /sw/vnu-18.7.23

rm -rf /sys_pay/sw/vnu-18.7.23
cp -r dist /sys_pay/sw/vnu-18.7.23

popd

rm -rf $B
