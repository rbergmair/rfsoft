HOSTNAME=`hostname --short`

_BUILDTYPE="generic"
MAKEOPTS="-j9"

CFLAGS=""

#if [ "x$HOSTNAME" == "xexamplehost" ]
#then
#  _BUILDTYPE="optimized"
#  MAKEOPTS="$MAKEOPTS -j9"
#fi

if [ "x$_BUILDTYPE" == "xoptimized" ]
then
  CFLAGS="-march=native $CFLAGS"
fi

CXXFLAGS="${CFLAGS}"

export _BUILDTYPE
export MAKEOPTS
export CFLAGS
export CXXFLAGS

export _CC="gcc"
export _MAKE="make"
export _INSTALL="install -c"

export _PYTHON_PREFIX="/sw/Python-3.6.5"
export _PYTHON="$_PYTHON_PREFIX/bin/python3.6"
export _PIP="$_PYTHON_PREFIX/bin/pip3.6"
export _PYTHON_SITEPKGS="$_PYTHON_PREFIX/lib/python3.6/site-packages"

export _PKGSRC="$PWD/../pkgsrc"
export _PKGSRCENC="$PWD/../pkgsrcenc"

export CC="$_CC $CFLAGS"

if ping -c1 www.google.com > /dev/null 2>/dev/null
then
  echo "this part of the build process needs to run without an internet"
  echo "connection.  please disconnect your internet and retry."
  exit -1
fi
