#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/citus-8.3.2-pg10.tar.gz -C $B || exit -1

pushd $B/citus-8.3.2/

PG_CONFIG=/sw/postgresql-10.19/bin/pg_config \
  ./configure \
    --prefix=/sw/citus-8.3.2-pg10 || exit -1

mv src/include/distributed/citus_nodes.h bla
cat bla \
  | sed 's/const char\*\* CitusNodeTagNames/extern const char** CitusNodeTagNames/g' \
  > src/include/distributed/citus_nodes.h

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
