#!/bin/bash

#
# see
#
#   http://www.scipy.org/scipylib/building/linux.html
#

source globals.sh
_PKGSRC=$_PKGSRC/native

unset MAKEOPTS
unset CC
#unset CFLAGS

OUTPUT_LIB="libamd.so
  libbtf.so
  libcamd.so
  libccolamd.so
  libcholmod.so
  libcolamd.so
  libcxsparse.so
  libklu.so
  libldl.so
  libmetis.so
  librbio.so
  libumfpack.so
  libspqr.so
  libsuitesparseconfig.so
  "

OUTPUT_INCLUDE="amd.h
  btf.h
  camd.h
  ccolamd.h
  cholmod_blas.h
  cholmod_camd.h
  cholmod_check.h
  cholmod_cholesky.h
  cholmod_complexity.h
  cholmod_config.h
  cholmod_core.h
  cholmod_function.h
  cholmod_gpu.h
  cholmod_gpu_kernels.h
  cholmod.h
  cholmod_io64.h
  cholmod_matrixops.h
  cholmod_modify.h
  cholmod_partition.h
  cholmod_supernodal.h
  cholmod_template.h
  colamd.h
  cs.h
  klu.h
  ldl.h
  metis.h
  RBio.h
  spqr.hpp
  SuiteSparse_config.h
  SuiteSparseQR_C.h
  SuiteSparseQR_definitions.h
  SuiteSparseQR.hpp
  umfpack_col_to_triplet.h
  umfpack_defaults.h
  umfpack_free_numeric.h
  umfpack_free_symbolic.h
  umfpack_get_determinant.h
  umfpack_get_lunz.h
  umfpack_get_numeric.h
  umfpack_get_symbolic.h
  umfpack_global.h
  umfpack.h
  umfpack_load_numeric.h
  umfpack_load_symbolic.h
  umfpack_numeric.h
  umfpack_qsymbolic.h
  umfpack_report_control.h
  umfpack_report_info.h
  umfpack_report_matrix.h
  umfpack_report_numeric.h
  umfpack_report_perm.h
  umfpack_report_status.h
  umfpack_report_symbolic.h
  umfpack_report_triplet.h
  umfpack_report_vector.h
  umfpack_save_numeric.h
  umfpack_save_symbolic.h
  umfpack_scale.h
  umfpack_solve.h
  umfpack_symbolic.h
  umfpack_tictoc.h
  umfpack_timer.h
  umfpack_transpose.h
  umfpack_triplet_to_col.h
  umfpack_wsolve.h"

OUTPUT_PREFIX=/sw/SuiteSparse-5.10.1

B=`mktemp -d`

mkdir -p $OUTPUT_PREFIX/lib || true
mkdir -p $OUTPUT_PREFIX/include || true

tar xfz $_PKGSRC/SuiteSparse-5.10.1.tar.gz -C $B || exit -1

pushd $B/SuiteSparse-5.10.1/

# MY_METIS_LIB=/sw/metis-5.1.0/lib/libmetis.a

export LDFLAGS="-L/sw/SuiteSparse-5.10.1/lib -L/sw/SuiteSparse-5.10.1/lib64"

OPTS="
  SUITESPARSE=/sw/SuiteSparse-5.10.1
  BLAS=/sw/atlas-3.10.3/lib/libtatlas.so
  LAPACK=/sw/atlas-3.10.3/lib/liblapack.a
  TBB=-ltbb"


$_MAKE $MAKEOPTS $OPTS library
$_MAKE $MAKEOPTS $OPTS install

for FILE in $OUTPUT_LIB
do
  if [ ! -e $OUTPUT_PREFIX/lib/$FILE ]
  then
    echo "detected corrupt build: $FILE missing"
    exit -1
  fi
done

for FILE in $OUTPUT_INCLUDE
do
  if [ ! -e $OUTPUT_PREFIX/include/$FILE ]
  then
    echo "detected corrupt build: $FILE missing"
    exit -1
  fi
done

cp -a $OUTPUT_PREFIX /sys_pay/sw

popd

rm -rf $B
