#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/google-leveldb-1.23.tar.gz -C $B || exit -1

pushd $B/leveldb-1.23

pushd third_party
rm -rf benchmark googletest
tar xf $_PKGSRC/google-benchmark-1.6.0.tar.gz
tar xf $_PKGSRC/google-googletest-1.11.0.tar.gz
mv benchmark-1.6.0 benchmark
mv googletest-release-1.11.0 googletest
popd

mkdir build
pushd build
cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .
popd

popd

rm -rf /sw/google-leveldb-1.23
cp -r $B/leveldb-1.23 /sw/google-leveldb-1.23

rm -rf /sys_pay/sw/google-leveldb-1.23
cp -r $B/leveldb-1.23 /sys_pay/sw/google-leveldb-1.23

rm -rf $B
