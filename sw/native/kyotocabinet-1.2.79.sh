#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

# export CFLAGS="-std=c99 ${CFLAGS}"
# export CXXFLAGS="-std=c++98 ${CXXFLAGS}"

B=`mktemp -d`

tar xfz $_PKGSRC/kyotocabinet-1.2.79.tar.gz -C $B || exit -1

pushd $B/kyotocabinet-1.2.79

./configure --prefix=/sw/kyotocabinet-1.2.79 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
