#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

mkdir -p /sw/Ink-3.1.10/
cp $_PKGSRC/Ink-3.1.10.zip /sw/Ink-3.1.10/

mkdir -p /sys_pay/sw/Ink-3.1.10/
cp $_PKGSRC/Ink-3.1.10.zip /sys_pay/sw/Ink-3.1.10/
