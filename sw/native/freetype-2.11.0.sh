#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/freetype-2.11.0.tar.xz -C $B || exit -1

pushd $B/freetype-2.11.0

./configure --prefix=/sw/freetype-2.11.0 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
