#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/ImageMagick-6.9.12-31.tar.xz -C $B || exit -1

pushd $B/ImageMagick-6.9.12-31

CONFIGOPTS="
  --with-jbig=yes
  --with-jpeg=yes
  --with-png=yes
  --with-tiff=yes
  --with-webp=yes
  --with-openjp2=yes
  --with-freetype=yes
  "

PKG_CONFIG_PATH=/sw/freetype-2.11.0/lib/pkgconfig \
  ./configure $CONFIGOPTS \
    --prefix=/sw/ImageMagick-6.9.12-31 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
