#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/gdal-3.4.0.tar.gz -C $B || exit -1

pushd $B/gdal-3.4.0

./configure \
  --prefix=/sw/gdal-3.4.0 \
  --with-proj=/sw/proj-8.1.1 \
  || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1
# cp port/cpl_vsi_error.h /sw/gdal-2.3.0/include
# cp port/cpl_vsi_error.h /sys_pay/sw/gdal-2.3.0/include

popd

rm -rf $B
