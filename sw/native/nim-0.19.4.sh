#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/nim-0.19.4.tar.xz -C $B || exit -1

pushd $B/nim-0.19.4

./build.sh
./bin/nim c koch
./koch tools

rm -rf /sw/nim-0.19.4
cp -r . /sw/nim-0.19.4

rm -rf /sys_pay/sw/nim-0.19.4
cp -r . /sys_pay/sw/nim-0.19.4

popd

rm -rf $B
