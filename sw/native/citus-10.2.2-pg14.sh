#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/citus-10.2.2-pg14.tar.gz -C $B || exit -1

pushd $B/citus-10.2.2/

PG_CONFIG=/sw/postgresql-14.1/bin/pg_config \
  ./configure \
    --prefix=/sw/citus-10.2.2-pg14 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
