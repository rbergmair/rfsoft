#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/geos-3.9.2.tar.bz2 -C $B || exit -1

pushd $B/geos-3.9.2

./configure --prefix=/sw/geos-3.9.2 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
