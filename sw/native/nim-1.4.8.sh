#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/nim-1.4.8.tar.xz -C $B || exit -1

pushd $B/nim-1.4.8

./build.sh
./bin/nim c koch
./koch tools

rm -rf /sw/nim-1.4.8
cp -r . /sw/nim-1.4.8

rm -rf /sys_pay/sw/nim-1.4.8
cp -r . /sys_pay/sw/nim-1.4.8

popd

rm -rf $B
