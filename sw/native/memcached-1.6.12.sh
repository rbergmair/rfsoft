#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xfz $_PKGSRC/memcached-1.6.12.tar.gz -C $B || exit -1

pushd $B/memcached-1.6.12/

./configure --prefix=/sw/memcached-1.6.12 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
