#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/postgresql-14.1.tar.bz2 -C $B || exit -1

pushd $B/postgresql-14.1/

./configure --prefix=/sw/postgresql-14.1 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE $MAKEOPTS world || exit -1
$_MAKE install || exit -1
$_MAKE install-world || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1
$_MAKE DESTDIR=/sys_pay install-world || exit -1

popd

rm -rf $B
