#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/zeromq-4.3.4.tar.gz -C $B || exit -1

pushd $B/zeromq-4.3.4

./configure --prefix=/sw/zeromq-4.3.4 || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
