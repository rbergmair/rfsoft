#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

B=`mktemp -d`

tar xf $_PKGSRC/nim-0.16.0.tar.gz -C $B || exit -1

pushd $B/nim-0.16.0

./build.sh
./bin/nim c koch
./koch tools

rm -rf /sw/nim-0.16.0
cp -r . /sw/nim-0.16.0

rm -rf /sys_pay/sw/nim-0.16.0
cp -r . /sys_pay/sw/nim-0.16.0

popd

rm -rf $B
