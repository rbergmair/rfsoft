#!/bin/bash

#
# see
#
#   http://math-atlas.sourceforge.net/atlas_install/node6.html
#   http://math-atlas.sourceforge.net/atlas_install/node27.html
#   http://math-atlas.sourceforge.net/atlas_install/node34.html
#   http://www.scipy.org/scipylib/building/linux.html
#  

source globals.sh
_PKGSRC=$_PKGSRC/native

OUTPUT_PREFIX="/sw/atlas-3.10.3"

OUTPUT_LIB="lib/liblapack.a
  lib/libptf77blas.a
  lib/libptcblas.a
  lib/libtatlas.so
  lib/libatlas.a
  lib/libf77blas.a
  lib/libsatlas.so
  lib/libcblas.a"

OUTPUT_INCLUDE="include/cblas.h
  include/clapack.h"

UNAME=`uname -m`
BITS=""
HOSTNAME=`hostname --short`

if [ "x$UNAME" == "xx86_64" ]
then
  BITS="64"
else

  if [ "x$UNAME" == "xi686" ]
  then
    BITS="32"
  else
    echo "can't figure this system out"
    exit -2
  fi

fi


B=`mktemp -d`

tar xfj $_PKGSRC/atlas3.10.3.tar.bz2 -C $B || exit -1

BB=`mktemp -d --tmpdir=$B/ATLAS`

pushd $BB

if [ "x$_BUILDTYPE" == "xgeneric" ]
then

echo "doing generic build"

#( head -n 1028 ../CONFIG/src/config.c; echo "   *ThrChk = 0;"; tail -n 349 ../CONFIG/src/config.c; ) > bla
#cp bla ../CONFIG/src/config.c

../configure \
  -b $BITS \
  -V 192 -A x86SSE2 -t 2 \
  -Fa alg \
  -fPIC \
  -D c -DPentiumCPS=2400 \
  --prefix=$OUTPUT_PREFIX \
  --with-netlib-lapack-tarfile=$_PKGSRC/lapack-3.10.0.tar.gz || exit -1

else

echo "doing optimized build"

#( head -n 1028 ../CONFIG/src/config.c; echo "   *ThrChk = 0;"; tail -n 349 ../CONFIG/src/config.c; ) > bla
#cp bla ../CONFIG/src/config.c

CPUFREQ=`cat /proc/cpuinfo | grep -e 'cpu MHz' | head -n1 | cut -d ':' -f 2 | cut -d '.' -f 1 | sed 's/ //g'`

../configure \
  -b $BITS \
  -Fa alg \
  -fPIC \
  -D c -DPentiumCPS=$CPUFREQ \
  --prefix=$OUTPUT_PREFIX \
  --with-netlib-lapack-tarfile=$_PKGSRC/lapack-3.10.0.tar.gz || exit -1 

fi

make

pushd lib

make shared
make ptshared

popd

make install


for FILE in $OUTPUT_LIB $OUTPUT_INCLUDE
do
  if [ ! -e $OUTPUT_PREFIX/$FILE ]
  then
    echo "detected corrupt build: $FILE missing"
    exit -1
  fi
done

cp -a $OUTPUT_PREFIX /sys_pay/sw

popd


rm -rf $B
