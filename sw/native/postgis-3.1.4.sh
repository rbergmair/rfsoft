#!/bin/bash

source globals.sh
_PKGSRC=$_PKGSRC/native

unset MAKEOPTS


B=`mktemp -d`

tar xf $_PKGSRC/postgis-3.1.4.tar.gz -C $B || exit -1

pushd $B/postgis-3.1.4/

./configure \
  --prefix=/sw/postgis-3.1.4-pg10 \
  --with-pgconfig=/sw/postgresql-10.19/bin/pg_config \
  --with-geosconfig=/sw/geos-3.9.2/bin/geos-config \
  --with-gdalconfig=/sw/gdal-3.4.0/bin/gdal-config \
  --with-projdir=/sw/proj-8.1.1 \
  --without-protobuf \
  || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B


B=`mktemp -d`

tar xf $_PKGSRC/postgis-3.1.4.tar.gz -C $B || exit -1

pushd $B/postgis-3.1.4/

./configure \
  --prefix=/sw/postgis-3.1.4-pg14 \
  --with-pgconfig=/sw/postgresql-14.1/bin/pg_config \
  --with-geosconfig=/sw/geos-3.9.2/bin/geos-config \
  --with-gdalconfig=/sw/gdal-3.4.0/bin/gdal-config \
  --with-projdir=/sw/proj-8.1.1 \
  --without-protobuf \
  || exit -1

$_MAKE $MAKEOPTS || exit -1
$_MAKE install || exit -1
$_MAKE DESTDIR=/sys_pay install || exit -1

popd

rm -rf $B
