#!/bin/bash

mkdir native >/dev/null 2>/dev/null || true
mkdir pip >/dev/null 2>/dev/null || true
mkdir java >/dev/null 2>/dev/null || true

DF=

DF="$DF https://sourceforge.net/projects/math-atlas/files/Stable/3.10.3/atlas3.10.3.tar.bz2|native/atlas3.10.3.tar.bz2"
DF="$DF https://github.com/Reference-LAPACK/lapack/archive/refs/tags/v3.10.0.tar.gz|native/lapack-3.10.0.tar.gz"
DF="$DF https://github.com/DrTimothyAldenDavis/SuiteSparse/archive/refs/tags/v5.10.1.tar.gz|native/SuiteSparse-5.10.1.tar.gz"
DF="$DF https://download.savannah.gnu.org/releases/freetype/freetype-2.11.0.tar.xz|native/freetype-2.11.0.tar.xz"

DF="$DF https://download.osgeo.org/proj/proj-8.1.1.tar.gz|native/proj-8.1.1.tar.gz"
DF="$DF https://github.com/OSGeo/gdal/releases/download/v3.4.0/gdal-3.4.0.tar.gz|native/gdal-3.4.0.tar.gz"
DF="$DF http://download.osgeo.org/geos/geos-3.9.2.tar.bz2|native/geos-3.9.2.tar.bz2"

# ImageMagick release model known to cause problems
DF="$DF https://download.imagemagick.org/ImageMagick/download/releases/ImageMagick-6.9.12-31.tar.xz|native/ImageMagick-6.9.12-31.tar.xz"
DF="$DF https://download.imagemagick.org/ImageMagick/download/releases/ImageMagick-7.1.0-16.tar.xz|native/ImageMagick-7.1.0-16.tar.xz"

DF="$DF https://www.memcached.org/files/memcached-1.6.12.tar.gz|native/memcached-1.6.12.tar.gz"

DF="$DF https://github.com/google/benchmark/archive/refs/tags/v1.6.0.tar.gz|native/google-benchmark-1.6.0.tar.gz"
DF="$DF https://github.com/google/googletest/archive/refs/tags/release-1.11.0.tar.gz|native/google-googletest-1.11.0.tar.gz"
DF="$DF https://github.com/google/leveldb/archive/refs/tags/1.23.tar.gz|native/google-leveldb-1.23.tar.gz"

DF="$DF https://dbmx.net/kyotocabinet/pkg/kyotocabinet-1.2.79.tar.gz|native/kyotocabinet-1.2.79.tar.gz"
DF="$DF https://github.com/zeromq/libzmq/releases/download/v4.3.4/zeromq-4.3.4.tar.gz|native/zeromq-4.3.4.tar.gz"

DF="$DF https://ftp.postgresql.org/pub/source/v10.19/postgresql-10.19.tar.bz2|native/postgresql-10.19.tar.bz2"
DF="$DF https://ftp.postgresql.org/pub/source/v14.1/postgresql-14.1.tar.bz2|native/postgresql-14.1.tar.bz2"
DF="$DF https://github.com/citusdata/citus/archive/refs/tags/v8.3.2.tar.gz|native/citus-8.3.2-pg10.tar.gz"
DF="$DF https://github.com/citusdata/citus/archive/refs/tags/v10.2.2.tar.gz|native/citus-10.2.2-pg14.tar.gz"
DF="$DF https://download.osgeo.org/postgis/source/postgis-3.1.4.tar.gz|native/postgis-3.1.4.tar.gz"

DF="$DF https://dev.gentoo.org/~gyakovlev/distfiles/icedtea-bin-core-3.16.0-amd64.tar.xz|native/icedtea-bin-core-3.16.0-amd64.tar.xz"

# disappeared
# DF="$DF https://nim-lang.org/download/nim-0.16.0.tar.gz|native/nim-0.16.0.tar.gz"
DF="$DF https://nim-lang.org/download/nim-0.18.0.tar.xz|native/nim-0.18.0.tar.xz"
DF="$DF https://nim-lang.org/download/nim-0.19.4.tar.xz|native/nim-0.19.4.tar.xz"
DF="$DF https://nim-lang.org/download/nim-1.4.8.tar.xz|native/nim-1.4.8.tar.xz"

DF="$DF https://github.com/sapo/Ink/archive/3.1.10.zip|native/Ink-3.1.10.zip"

DF="$DF https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tar.xz|native/Python-3.10.0.tar.xz"



for FILEN in $DF
do
  FURL=`echo $FILEN | cut -d '|' -f 1`
  FFN=`echo $FILEN | cut -d '|' -f 2`
  wget -O $FFN $FURL || exit -1
done

pushd java
for VAL in `find -type d | grep -v '^\.$' | grep -v '^\.\.$'`
do
  tar c ${VAL} > ${VAL}.tar
  gzip ${VAL}.tar
  rm -rf ${VAL}
done
popd
