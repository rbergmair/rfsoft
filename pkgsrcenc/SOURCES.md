# Instructions For Obtaining Encumbered Packages

* go to <http://www.oracle.com/technetwork/java/javase/downloads/index.html>
  and download `jdk-10.0.1_linux-x64_bin.tar.gz`, accepting the EULA
  and put it under `native`.
