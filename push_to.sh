#!/bin/bash

rsync \
  -ave 'ssh' \
  --exclude 'pkgsrc/*/*' \
  --exclude 'pkgsrcenc/*/*' \
  ./ root@$1:/root/rfsoft/

rsync \
  -ave 'ssh' \
  ./docker/ root@$1:/root/rfsofttmp/

#rsync \
#  -ave 'ssh' \
#  . root@$1:/root/rfsoft
