# Creating an RFSOFT Distribution


## Creating Tarballs

This needs to be done in a state where the chroot environments are not
mounted, i.e. there should be no bind-mounts in place.  To achieve this, just
reboot your machine.

Then you need to sanitize the environments.  For example, the sshd private
keys under `/etc/ssh/*key*` files should never be among the files that you
redistribute.  Similarly, the `/etc/shadow` password database should always
be kept private, etc. etc.  Use the script `31_chroots_sanitize.sh`, but also
use your brain, please!

Then:
```
mkdir /root/rfsofttmp
```

Then the following steps are necessary (assuming today's date is 20180829):
```
cd /sys_pay
tar --exclude='./sw' --create '.' > /root/rfsofttmp/rfsoft-pay.tar
tar --create './sw' > /root/rfsofttmp/rfsoft-pay-sw.tar
cd /sys_stg
tar --exclude='./sw' --create '.' > /root/rfsofttmp/rfsoft-stg.tar
tar --create './sw' > /root/rfsofttmp/rfsoft-stg-sw.tar
```

Then, in the `rfsoft` project directory,
```
cd pkgsrc
tar c native pip > /root/rfsofttmp/rfsoft-pkgsrc.tar
```


## (Optional) Dockerizing

To import the base image, build the final image, and run a container
based off of it do this:
```
cd /root/rfsofttmp
bash ./docker_import.sh
bash ./docker_build.sh
bash ./docker_run.sh
```

Then check that everything is set up as expected by doing
```
docker ps
docker exec -t -i rfsoft /bin/bash
supervisorctl status
```


## (Optional) Pushing the Docker Image to Amazon Elastic Container Registry (ECR)

Check out
* https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
* https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html

Run
```
aws ecr create-repository --repository-name rfsoft
```

This will give you output
```
{
    "repository": {
        "registryId": "aws_account_id",
        "repositoryName": "rfsoft",
        "repositoryArn": "arn:aws:ecr:eu-central-1:aws_account_id:repository/rfsoft",
        "createdAt": 1505337806.0,
        "repositoryUri": "aws_account_id.dkr.ecr.eu-central-1.amazonaws.com/rfsoft"
    }
}
```

Then run
```
docker tag rfsoft aws_account_id.dkr.ecr.eu-central-1.amazonaws.com/rfsoft
```

Then
```
aws ecr get-login --no-include-email
```

Then run the docker login command you just got as output in the previous step.

Finally
```
docker push aws_account_id.dkr.ecr.eu-central-1.amazonaws.com/rfsoft
```
