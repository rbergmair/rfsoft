# Installation From Tarball For Use Of RFSOFT With Chroot


## Preparing the Host


### Preliminaries (Usually, no Action Required)

* When chrooting into RFSOFT, the following file system locations have special
  meaning (think "volumes" in docker).

    - `/sys_pay` is the location of the RFSOFT payload system.
    - `/sys_stg` is the location of the RFSOFT staging system.
    - `/dta` is a shared loction for data.
    - `/mass` is a shared location for mass data.
    - Furthermore, the directories `/home`, `/root`, `/tmp`, `/sys`, `/dev`, 
      `/run`, `/var/run`, and `/media` are shared, usage being assumed to
      be compatible with the Linux Standard Base (LSB) definitions.

* It can be useful to put `/sys_pay` and `/sys_stg` on separate volumes,
  using a higher inode density, e.g.
  ```
  mkfs.ext4 -i 8192 /dev/vg/00/sys_pay
  ```

* Make sure there's enough space on `/mass`, if you'll be compiling the
  `sw` environment, and mongodb in particular (last time this failed
  on 20G of free space, but succeeded on 40G) as `/mass/tmp` is used as a
  temporary build location for packages that require a lot of space to build.

* The guest system relies on the host system for keeping the clock up to
  date.  It's a good idea to have an ntpd or a htpdate daemon, or something
  like that running on the host system.

* The guest system relies on the host system for its user database.
    - The `daemon` user specified as part of the LSB is used by RFSOFT to run
      unprivileged processes such as database servers etc.  Despite this user
      and this practice being marked as "legacy" by LSB, make sure that this
      user does exist on the host system.
    - The `portage` user and `portage` group are used by Gentoo portage and
      are thus required for executing these build steps but not necessarily
      for running the payload system.
      If necessary, insert line
      ```
      portage:x:250:250:portage:/var/tmp/portage:/bin/false
      ```
      into `/etc/passwd` and
      ```
      portage::250:portage
      ```
      into `/etc/group`.


### Creating a Payload User (Optional)

RFSOFT tries to establish a convention in the RFSOFT environment to have a
user called `pay` which is a system user, similar to `daemon`, used to run
things like overnight batches, server processes etc.  The user `pay` is
different from `daemon` however, in that `pay` is intended for running the
actual payload of the RFSOFT system.  Since RFSOFT generally shares the user
system with the host system, such a user needs to be created on the host, not
the RFSOFT guest.

Use your distribution's userfriendly variation of `useradd` to do this.  On
ubuntu, it's called `adduser`:
```
# adduser pay
[...]
# su pay
```


### Fixing Users' Bash Profile (Ubuntu Hosts Only)

For users that are going to be shared between the host system and RFSOFT system, have a look in their `~/.bashrc`.
If there is a line like
```
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
```
then comment it out to read
```
# [ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
```
as this conflicts with the Gentoo system used for RFSOFT.  This goes for the
`pay` user and any other user that will be using RFSOFT.


## Installing From Tarball

As root, in the RFSOFT directory, do this:
```
cd sys
bash ./01_chroots_init.sh
```

Then, still as root, in a directory that has the tarballs, do something along
the following lines:
```
tar xf rfsoft-pay-20180627.tar.xz -C /sys_pay
tar xf rfsoft-pay-sw-20180627.tar.xz -C /sys_pay
tar xf rfsoft-stg-20180627.tar.xz -C /sys_stg
tar xf rfsoft-stg-sw-20180627.tar.xz -C /sys_stg
```

Then, again in the RFSOFT directory, still as root, do this:
```
cd sys
bash ./03_chroots_mount.sh
```

