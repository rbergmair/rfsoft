# Building RFSOFT


## Preparing the Host


### Preliminaries (Usually, no Action Required)

* When chrooting into RFSOFT, the following file system locations have special
  meaning (think "volumes" in docker).

    - `/sys_pay` is the location of the RFSOFT payload system.
    - `/sys_stg` is the location of the RFSOFT staging system.
    - `/dta` is a shared loction for data.
    - `/mass` is a shared location for mass data.
    - Furthermore, the directories `/home`, `/root`, `/tmp`, `/sys`, `/dev`, 
      `/run`, `/var/run`, and `/media` are shared, usage being assumed to
      be compatible with the Linux Standard Base (LSB) definitions.

* It can be useful to put `/sys_pay` and `/sys_stg` on separate volumes,
  using a higher inode density, e.g.
  ```
  mkfs.ext4 -i 8192 /dev/vg/00/sys_pay
  ```

* Make sure there's enough space on `/mass`, if you'll be compiling the
  `sw` environment, and mongodb in particular (last time this failed
  on 20G of free space, but succeeded on 40G) as `/mass/tmp` is used as a
  temporary build location for packages that require a lot of space to build.

* The guest system relies on the host system for keeping the clock up to
  date.  It's a good idea to have an ntpd or a htpdate daemon, or something
  like that running on the host system.

* The guest system relies on the host system for its user database.
    - The `daemon` user specified as part of the LSB is used by RFSOFT to run
      unprivileged processes such as database servers etc.  Despite this user
      and this practice being marked as "legacy" by LSB, make sure that this
      user does exist on the host system.
    - The `portage` user and `portage` group are used by Gentoo portage and
      are thus required for executing these build steps but not necessarily
      for running the payload system.
      If necessary, insert line
      ```
      portage:x:250:250:portage:/var/tmp/portage:/bin/false
      ```
      into `/etc/passwd` and
      ```
      portage::250:portage
      ```
      into `/etc/group`.


### Creating a Payload User (Optional)

RFSOFT tries to establish a convention in the RFSOFT environment to have a
user called `pay` which is a system user, similar to `daemon`, used to run
things like overnight batches, server processes etc.  The user `pay` is
different from `daemon` however, in that `pay` is intended for running the
actual payload of the RFSOFT system.  Since RFSOFT generally shares the user
system with the host system, such a user needs to be created on the host, not
the RFSOFT guest.

Use your distribution's userfriendly variation of `useradd` to do this.  On
ubuntu, it's called `adduser`:
```
# adduser pay
[...]
# su pay
```


### Fixing Users' Bash Profile (Ubuntu Hosts Only)

For users that are going to be shared between the host system and RFSOFT system, have a look in their `~/.bashrc`.
If there is a line like
```
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
```
then comment it out to read
```
# [ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
```
as this conflicts with the Gentoo system used for RFSOFT.  This goes for the
`pay` user and any other user that will be using RFSOFT.


## Building


### Phase 0: Push Repo

To push this repo to a server that is going to serve for the RFSOFT build, to the following:
```
# bash ./push_to.sh rfsoft-build
```
This will `rsync` the current working directory out to the `rfsoft` subdirectory of root's home on the host `rfsoft-build`.  If you already
are root on the machine that is going to be building this, then there's
nothing to do here.

### Phase 1: Init Chroots

In the `RFSOFT` project directory, do the following:
```
# cd ~/rfsoft/sys
# ./01_chroots_init.sh
```

Then download and unpack gentoo stages and portage:
```
# cd /tmp
# wget http://distfiles.gentoo.org/releases/amd64/autobuilds/20180603T214502Z/stage3-amd64-20180603T214502Z.tar.xz
...
# wget http://distfiles.gentoo.org/snapshots/portage-20180604.tar.xz
...
# tar xf stage3-amd64-20180603T214502Z.tar.xz -C /sys_stg --xattrs-include='*.*' --numeric-owner
# tar xf portage-20180604.tar.xz -C /sys_stg/usr --xattrs-include='*.*' --numeric-owner
```

Review the configuration files under `sys/chroots`.
In particular the setting `MAKEOPTS` under `etc/portage/make.conf`
is optimized to run on an octacore host system.

Then do the following to place the configuration files into the
guest filesystems.
```
# cd ~/rfsoft/sys
# ./02_chroots_setup.sh
...
```


### Phase 2: Initial Setup of Gentoo Base System

Then do the following
```
# cd ~/rfsoft/sys
# ./03_chroots_mount.sh 
# chroot /sys_stg
# env-update
>>> Regenerating /etc/ld.so.cache...
# source /etc/profile
(RFSOFT stg) rfsoft-build / # cd
(RFSOFT stg) rfsoft-build ~ #
```

Generate the locale.
```
(RFSOFT stg) # locale-gen
...
(RFSOFT stg) # env-update && source /etc/profile
...
(RFSOFT stg) #
```

Update portage, and re-generate locale:
```
(RFSOFT stg) # emerge-webrsync
...
(RFSOFT stg) # emerge --sync
...
(RFSOFT stg) # emerge --update portage
...
(RFSOFT stg) # emerge --config sys-libs/timezone-data
...
(RFSOFT stg) # locale-gen
...
(RFSOFT stg) # env-update && source /etc/profile
...
```

Install a kernel and select it:
```
(RFSOFT stg) # emerge --update vanilla-sources
...
(RFSOFT stg) # eselect kernel list
Available kernel symlink targets:
  [1]   linux-5.15.3
(RFSOFT stg) # eselect kernel set 1
(RFSOFT stg) # eselect kernel list
Available kernel symlink targets:
  [1]   linux-5.15.3 *
(RFSOFT stg) #
```

When building on a server, it is recommended that the phases from here on out
be executed in GNU screen or TMUX or a tool like that, since they'll take
a long time to execute, and terminal output might be vital in debugging
any problems.
```
(RFSOFT stg) # emerge app-misc/screen
...
(RFSOFT stg) # screen
```


### Phase 3: Emerge the Gentoo Base System

This is how easy it should be (in theory...)
```
(RFSOFT stg) # cd ~/rfsoft/sys
(RFSOFT stg) # emerge --verbose --update --deep --newuse @world
...
(RFSOFT stg) # ./11_emerge_sys_run.sh
...
(RFSOFT stg) # ./12_emerge_sys_dev.sh
...
(RFSOFT stg) # ./13_emerge_sys_stg.sh
...
(RFSOFT stg) # ./14_npm_sys_dev.sh
...
(RFSOFT stg) # ./15_gem_sys_dev.sh
...
(RFSOFT stg) # ./21_chroots_postsetup.sh
(RFSOFT stg) # exit
```


### Phase 4: `sys_pay` Postsetup

```
# chroot /sys_pay
# source /etc/profile
# env-update
# source /etc/profile
(RFSOFT) # cd
(RFSOFT) # locale-gen
(RFSOFT) # cd ~/rfsoft/sys
(RFSOFT) # ./14_npm_sys_dev.sh
...
(RFSOFT) # ./15_gem_sys_dev.sh
...
(RFSOFT) # exit
```


### Phase 5: Build `/sw`

#### Turn Off Dynamic CPU Frequency Scaling

Part of the `sw` build is atlas, which needs to have cpu throttling or any
kind of dynamic cpu frequency scaling turned off.

To initially get your bearings do this:
```
cpufreq-info
```

When I last did this on a Hetzner cloud instance, I got
`no or unknown cpufreq driver is active on this CPU` and it turned out
to be fine.

Otherwise, for example when building on a laptop on bare metal, it might be
useful to use the kernel option
`intel_pstate=disable`, to `rmmod intel_powerclamp`, and to do something
along the lines of `cpupower frequency-set -g userspace` plus
`cpupower frequency-set -f 2.60GHz` or something like that.  Some research
will be necessary to find out how to do this in your environment.  If you
don't successfully disable this, then the atlas build process will
complain, so it's worth just having a shot to see if it works without
taking any particular steps.

#### Disable Internet Connection

This part of the build process has to run without any internet connection,
to ensure that any build scripts from packages we're using aren't trying
to be helpful and pulling in stuff over the internet without us
noticing.

If you have no way of physically cutting the connection (e.g. because you're
building on a server and are relying on SSH to work with the machine),
the following might be useful.  This will prevent the machine from
initiating any outbound connections, but will still allow the machine
to respond to inbound connections.

```
# iptables -t filter -I OUTPUT 1 -m state --state NEW -j DROP
# wget http://www.google.com/
--2021-11-23 11:03:12--  http://www.google.com/
Resolving www.google.com (www.google.com)... failed: Temporary failure in name resolution.
wget: unable to resolve host address ‘www.google.com’

```

#### Kick Off Build Process

```
(RFSOFT) # cd ~/rfsoft/pkgsrc
(RFSOFT) # ./download_from_mirror.sh
(RFSOFT) # cd ~/rfsoft/pkgsrcenc
(RFSOFT) # ./download_from_mirror.sh
(RFSOFT) # cd ~/rfsoft/sw
(RFSOFT) # ./
...
